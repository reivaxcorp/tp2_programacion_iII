# Tp2 Programacion III

El objetivo del trabajo pr´actico es medir la diferencia en los tiempos de ejecuci´on del Algoritmo
de Kruskal para el problema de ´arbol generador m´ınimo con y sin el uso de la estructura de
datos Union-Find. Para esto, se piden los siguientes puntos:

1. Implementar el Algoritmo de Kruskal sin Union-Find (es decir, determinando con BFS
si dos v´ertices est´an en la misma componente conexa).
2. Implementar el Algoritmo de Kruskal con alguna de las versiones de Union-Find, implementada sobre un arreglo de ´ındices.
3. Generar grafos aleatoriamente y medir el tiempo de ejecuci´on de las dos versiones.
4. Graficar el tiempo promedio de ejecuci´on en funci´on del tama˜no del grafo.
